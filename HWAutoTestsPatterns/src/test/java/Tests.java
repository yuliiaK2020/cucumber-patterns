import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Tests {
    @Before
    public void before(){
        WebDriverManager.chromedriver().setup();
    }

   @After
    public void after(){
        WebDriverST.getDriver().quit();
    }
    @Test
    public  void loginWriteAndCheckQuantity(){
        LoginPage loginPage = new LoginPage();

        loginPage.open();
        loginPage.getLoginInput().click();
        loginPage.getLoginInput().sendKeys("eskimo2020@i.ua");
        loginPage.getPasswordInput().click();
        loginPage.getPasswordInput().sendKeys("lemon2020");
        loginPage.getSubmitButton().click();
        new WebDriverWait(WebDriverST.getDriver(),15).until(ExpectedConditions.visibilityOf
                (new MainPage().getComposeLink()));
        MainPage mainPage = new MainPage();
        int lettersQuantityBefore = Integer.parseInt(mainPage.getLettersQuantity().getText());
        System.out.println(lettersQuantityBefore);
        mainPage.getComposeLink().click();
        new WebDriverWait(WebDriverST.getDriver(),5).until(ExpectedConditions.visibilityOf
                (new ComposePage().getSendButton()));
        ComposePage composePage=new ComposePage();
        composePage.getRecipient().click();
        composePage.getRecipient().sendKeys("eskimo2020@i.ua");
        composePage.getSubject().click();
        composePage.getSubject().sendKeys("test letter");
        composePage.getLetterText().click();
        composePage.getLetterText().sendKeys("Lets check how it's work!!!");
        composePage.getSendButton().click();
        new WebDriverWait(WebDriverST.getDriver(),10).until(ExpectedConditions.visibilityOf
                (new MainPage().getLettersQuantity()));
        int lettersQuantityAfter = Integer.parseInt(mainPage.getLettersQuantity().getText());
        System.out.println(lettersQuantityAfter);
        Assert.assertEquals("It's impossible to count quantity of letters",
                lettersQuantityBefore+1, lettersQuantityAfter);

  }
    @Test
    public void test_userNameExisted() {
        LoginPage loginPage = new LoginPage();
        loginPage.open();
        loginPage.getLoginInput().click();
        loginPage.getLoginInput().sendKeys("eskimo2020@i.ua");
        loginPage.getPasswordInput().click();
        loginPage.getPasswordInput().sendKeys("lemon2020");
        loginPage.getSubmitButton().click();
        new WebDriverWait(WebDriverST.getDriver(),15).until(ExpectedConditions.visibilityOf
                (new MainPage().getComposeLink()));
        MainPage mainPage=new MainPage();
        String actual = mainPage.getUserName().getText();
        Assert.assertEquals("Can not read name of user", "Eskimo", actual);
    }
    @Test
    public void addNoteAndCheckItMame() {
        LoginPage loginPage = new LoginPage();
        loginPage.open();
        loginPage.getLoginInput().click();
        loginPage.getLoginInput().sendKeys("eskimo2020@i.ua");
        loginPage.getPasswordInput().click();
        loginPage.getPasswordInput().sendKeys("lemon2020");
        loginPage.getSubmitButton().click();
        new WebDriverWait(WebDriverST.getDriver(),15).until(ExpectedConditions.visibilityOf
                (new MainPage().getComposeLink()));
        MainPage mainPage= new MainPage();
        mainPage.getContacts().click();
        new WebDriverWait(WebDriverST.getDriver(),10).until(ExpectedConditions.visibilityOf
                (new ContactPage().getNotes()));
        ContactPage contactPage = new ContactPage();
        contactPage.getNotes().click();
        new WebDriverWait(WebDriverST.getDriver(),10).until(ExpectedConditions.visibilityOf
                (new NotesPage().getCreateNewNote()));
        NotesPage notesPage=new NotesPage();
        notesPage.getCreateNewNote().click();
        notesPage.getInputText().click();
        notesPage.getInputText().sendKeys("New Note for test");
        notesPage.getCategory().click();
        notesPage.getCreateBTN().click();
        WebDriverST.getDriver().navigate().refresh();
        new WebDriverWait(WebDriverST.getDriver(),10).until(ExpectedConditions.visibilityOf
                (new NotesPage().getNameOfNote()));
        String actual = notesPage.getNameOfNote().getText();
        Assert.assertEquals("Can not check name of the last note","New Note for test", actual);
    }

    @Test
    public  void deleteAndCheckQuantity(){
        LoginPage loginPage = new LoginPage();

        loginPage.open();
        loginPage.getLoginInput().click();
        loginPage.getLoginInput().sendKeys("eskimo2020@i.ua");
        loginPage.getPasswordInput().click();
        loginPage.getPasswordInput().sendKeys("lemon2020");
        loginPage.getSubmitButton().click();
        new WebDriverWait(WebDriverST.getDriver(),15).until(ExpectedConditions.visibilityOf
                (new MainPage().getComposeLink()));
        MainPage mainPage = new MainPage();
        int lettersQuantityBefore = Integer.parseInt(mainPage.getLettersQuantity().getText());
        System.out.println(lettersQuantityBefore);

        mainPage.getChooseFirstLetter().click();
        new WebDriverWait(WebDriverST.getDriver(),15).until(ExpectedConditions.visibilityOf
                (new MainPage().getMark()));
        mainPage.getMark().click();
        new WebDriverWait(WebDriverST.getDriver(),15).until(ExpectedConditions.visibilityOf
                (new MainPage().getAsREad()));
        mainPage.getAsREad().click();
        WebDriverST.getDriver().navigate().refresh();
        new WebDriverWait(WebDriverST.getDriver(),15).until(ExpectedConditions.visibilityOf
                (new MainPage().getLettersQuantity()));

        int lettersQuantityAfter = Integer.parseInt(mainPage.getLettersQuantity().getText());
        System.out.println(lettersQuantityAfter);
        Assert.assertEquals("It's impossible to count quantity of letters",
                lettersQuantityBefore-1, lettersQuantityAfter);


    }
}
