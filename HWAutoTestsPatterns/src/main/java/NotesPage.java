import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class NotesPage {
    NotesPage (){
        PageFactory.initElements(WebDriverST.getDriver(), this);
    }

    @FindBy(css="body > div.body_container > div.Dimension.notes > div.Column.L75 > div > a.addNote")
    private WebElement createNewNote;

    @FindBy(css="#noteText")
    private WebElement inputText;
    @FindBy(css="[class='notes catlist'] [name='comment'] [value='423670']")
    private WebElement category;
    @FindBy(css="[class='Ubutton'] ")
    private WebElement createBTN;

    @FindBy(css="body > div.body_container > div.Dimension.notes > div.Column.L75 > div > div.noteContainer > div:nth-child(2) > p ")
    private WebElement nameOfNote;





}
