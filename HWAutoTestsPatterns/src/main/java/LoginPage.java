
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class LoginPage {
    LoginPage (){
        PageFactory.initElements(WebDriverST.getDriver(), this);
    }

   @FindBy (css="[name='lform'] [name='login']")
    private WebElement loginInput;

    @FindBy (css="[name='lform'] [name='pass']")
    private WebElement passwordInput;

    @FindBy (css="[name='lform'] [tabindex='4']")
    private WebElement submitButton;

    public  void  open (){
        WebDriverST.getDriver().get("https://mail.i.ua/");
    }
  }
