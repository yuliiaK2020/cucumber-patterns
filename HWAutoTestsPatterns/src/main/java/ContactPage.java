import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter
public class ContactPage {
    ContactPage (){
        PageFactory.initElements(WebDriverST.getDriver(), this);
    }
    @FindBy(css="body > div.body_container > div.section_nav > ul > li:nth-child(4) > a")
    private WebElement notes;
}
