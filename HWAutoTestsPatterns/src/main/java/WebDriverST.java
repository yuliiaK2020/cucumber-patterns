import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverST {
    private  static WebDriver driver;

    private  WebDriverST (){
        new ChromeDriver();
    }

    public static WebDriver getDriver(){
        if (driver== null){
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }
        return driver;
    }

}

